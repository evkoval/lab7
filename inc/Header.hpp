#pragma once
#define N 100
namespace ek {
	void z_mas(int n, int mas[N][N]);

	bool simple(int i, int n, int mas[N][N]);

	bool sum0(int i, int n, int mas[N][N]);

	int sum_stolb(int j, int n, int mas[N][N]);

	void sort(int n, int mas[N][N]);

	void vyvod(int n, int mas[N][N]);

	void Read(int& n, int mas[N][N]);

	void obr(int n, int mas[N][N]);
}